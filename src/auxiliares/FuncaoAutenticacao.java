package auxiliares;

import interfaces.PermitirAcesso;

/* REALMENTE E SOMENTE RECEBER ALGUEM QUE TEM O CONTRATO DA INTERFACE DE PermitirAcesso e CHAMAR O AUTENTICAR*/
public class FuncaoAutenticacao {

	private PermitirAcesso acesso;
	

	public FuncaoAutenticacao(PermitirAcesso acesso) {
		this.acesso = acesso;
	}
	
	public FuncaoAutenticacao() {
	}
	
	public boolean autenticarCursoJava(PermitirAcesso acesso) {
		return acesso.autenticar();
	}
	
	public boolean autenticarLogin() {
		return acesso.autenticar();
	}

}
